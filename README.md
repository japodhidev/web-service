# web-service

A [Symfony](https://symfony.com/) (web service) calculator app. 

## Description

Takes in arguments passed through vanity URL parameters. The URL scheme is as follows:

* /calculate/4/5/add/ - where `calculate` is the name of the service, `4` & `5` are the operands and `add` is the operation.

Sample operations include :

* add
* subtract
* multiply
* divide - _including a check for division by zero_

Responses are in JSON format, with views available for the authentication endpoints.

Additional authentication functionality is also available at the following endpoints:

* /register
* /login
* /logout

Authentication service is only available after a database backend has been configured.

`bitbucket-pipelines.yml` - BitBucket pipeline configuration file for deploying to [Heroku](https://www.heroku.com/).