<?php

namespace App\Service;

class CalculatorService
{
    /**
     * @param $a - Second url parameter after /calculate (integer)
     * @param $b - Third url parameter (integer)
     * @param $operation - Fourth parameter (string)
     * @return float|int|string
     */
    public function vuvuzela($a, $b, $operation) {
        $result;
        try {
            switch ($operation) {
                case 'add':
                    $result = $a + $b;
                    break;
                case 'subtract':
                    $result = $a - $b;
                    break;
                case 'multiply':
                    $result = $a * $b;
                    break;
                case 'divide':
                    if ($b == 0) {
                        $result = "Division by zero. Exiting.";
                        break;
                    } else {
                        $result = $a / $b;
                    }
                    break;
                default:
                    $result = "Operation is probably not permitted. Please try again";
            }

            return $result;
        } catch (\ErrorException $errorException){
            $result = "Error: " . $errorException->getMessage();
            return $result;
        }
    }
}