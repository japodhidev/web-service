<?php
namespace App\Service;

use App\Entity\DatabasePush;
use Doctrine\ORM\EntityManagerInterface;

class DatabasePushService
{
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    /**
     * @param $input
     * @return int
     */
    public function databasePush($input) {
        $result;
        $push = new DatabasePush();
        $push->setJina($input);

        try {
            $em = $this->em;
            $em->persist($push);
            $em->flush();
            $result = 1;
            return $result;
        } catch (\Exception $exception) {
            $result = "Error: " . $exception;
            return $result;
        }
    }

}