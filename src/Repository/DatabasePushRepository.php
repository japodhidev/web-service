<?php

namespace App\Repository;

use App\Entity\DatabasePush;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DatabasePush|null find($id, $lockMode = null, $lockVersion = null)
 * @method DatabasePush|null findOneBy(array $criteria, array $orderBy = null)
 * @method DatabasePush[]    findAll()
 * @method DatabasePush[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DatabasePushRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DatabasePush::class);
    }

    // /**
    //  * @return DatabasePush[] Returns an array of DatabasePush objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DatabasePush
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
