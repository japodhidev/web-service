<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DatabasePushRepository")
 */
class DatabasePush
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jina;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJina(): ?string
    {
        return $this->jina;
    }

    public function setJina(string $jina): self
    {
        $this->jina = $jina;

        return $this;
    }
}
