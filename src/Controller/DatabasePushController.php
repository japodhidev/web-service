<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManagerInterface;

use App\Service\DatabasePushService;

class DatabasePushController extends AbstractController
{
    /**
     * @Route("/database/push/{jina}", name="database_push")
     * @param $pushService - Service to push $jina to the database.
     * @return JsonResponse
     */
    public function index(DatabasePushService $pushService, string $jina)
    {
        $result = $pushService->databasePush($jina);
        $response = new JsonResponse([
            'input' => $jina,
            'result' => $result
        ]);
        return $response;
    }
}
