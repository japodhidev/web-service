<?php

namespace App\Controller;

use App\Service\CalculatorService;
Use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CalculatorServiceController extends AbstractController
{
    /**
     * @Route("/calculate/{a}/{b}/{operation}/")
     *
     * @param mixed $calcService - Calculator service
     * @param integer $a , $b - integers to be operated on
     * @param string $operation - the opration to be carried out
     * @return JsonResponse
     */
    public function index(CalculatorService $calcService, $a, $b, string $operation) {

        $answer = $calcService->vuvuzela($a, $b, $operation);
        $response = new JsonResponse([
            'a' => $a,
            'b' => $b,
            'output' => $answer
        ]);
        return $response;
    }

    /**
     * @Route("/calculate", name="calculate")
     * @param CalculatorService $calcService
     * @return JsonResponse
     */
    public function calculate(CalculatorService $calcService)
    {
        $request = Request::createFromGlobals();

        $a = $request->request->get('a');
        $b = $request->request->get('b');
        $operation = $request->request->get('operation');
        return $this->index($calcService, $a, $b, $operation);
//        $response = new JsonResponse([
//            'a' => $a,
//            'b' => $b,
//            'output' => $operation
//        ]);
//        return $response;
    }

}